package com.enrico.pokemonapp.module

import com.enrico.pokemonapp.data.AppRepository
import org.koin.dsl.module

val repositoryModule = module {
    single {
        AppRepository(get(), get())
    }
}