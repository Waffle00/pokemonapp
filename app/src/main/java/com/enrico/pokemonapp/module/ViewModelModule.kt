package com.enrico.pokemonapp.module

import androidx.paging.ExperimentalPagingApi
import com.enrico.pokemonapp.ui.MainViewModel
import org.koin.dsl.module

@ExperimentalPagingApi
val viewModelModule = module {
    single { MainViewModel(get()) }
}