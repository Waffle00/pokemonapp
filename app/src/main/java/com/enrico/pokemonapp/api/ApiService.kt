package com.enrico.pokemonapp.api

import com.enrico.pokemonapp.data.response.CardListResponse
import com.google.android.material.snackbar.BaseTransientBottomBar.BaseCallback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("v2/cards")
    suspend fun getCardList(
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ) : Response<CardListResponse>
}