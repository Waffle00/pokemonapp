package com.enrico.pokemonapp.data.paging

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.TypeConverter
import androidx.room.withTransaction
import com.enrico.pokemonapp.api.ApiService
import com.enrico.pokemonapp.data.response.DataItem
import com.enrico.pokemonapp.local.LocalDatabase
import com.enrico.pokemonapp.local.entity.CardEntity
import com.enrico.pokemonapp.local.entity.KeyEntity
import com.google.gson.Gson
import okio.IOException
import retrofit2.HttpException

@ExperimentalPagingApi
class CardRemoteMediator(
    private val api: ApiService,
    private val db: LocalDatabase
) : RemoteMediator<Int, CardEntity>() {

    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, CardEntity>
    ): MediatorResult {
        val pageKeyData = getKeyPageData(loadType, state)
        val page = when (pageKeyData) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }

        try {
            val response = api.getCardList(page = page, pageSize = 20)
            val isEndOfList = response.body()?.data?.isEmpty() ?: false
            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    db.cardDao().deleteAll()
                    db.keyDao().deleteAll()
                }
                val prevKey = if (page == 1) null else page - 1
                val nextKey = if (isEndOfList) null else page + 1
                val dataCard = response.body()?.data?.map {
                    CardEntity(id = it.id ?: "",
                        name = it.name,
                        superType = it.supertype,
                        types = fromArrayList(it.types),
                        artist = it.artist,
                        hp = it.hp,
                        level = it.level,
                        imageSmall = it.images?.small,
                        imageHigh = it.images?.large)
                }
                val keys = response.body()?.data?.map {
                    KeyEntity(id = it.id ?: "", prevKey = prevKey, nextKey = nextKey)
                }
                Log.e("TAG", "load: $dataCard", )
                db.cardDao().insertCardList(dataCard ?: listOf())
                db.keyDao().insertKeyList(keys ?: listOf())
            }
            return MediatorResult.Success(endOfPaginationReached = isEndOfList)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getKeyPageData(loadType: LoadType, state: PagingState<Int, CardEntity>): Any {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: 1
            }
            LoadType.APPEND -> {
                val remoteKeys = getLastRemoteKey(state)
                val nextKey = remoteKeys?.nextKey
                return nextKey ?: MediatorResult.Success(endOfPaginationReached = false)
            }
            LoadType.PREPEND -> {
                val remoteKeys = getFirstRemoteKey(state)
                val prevKey = remoteKeys?.prevKey ?: return MediatorResult.Success(
                    endOfPaginationReached = false
                )
                prevKey
            }
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, CardEntity>): KeyEntity? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                db.keyDao().getKeyId(repoId)
            }
        }
    }

    private suspend fun getLastRemoteKey(state: PagingState<Int, CardEntity>): KeyEntity? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { cat -> db.keyDao().getKeyId(cat.id ?: "") }
    }

    private suspend fun getFirstRemoteKey(state: PagingState<Int, CardEntity>): KeyEntity? {
        return state.pages
            .firstOrNull { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { cat -> db.keyDao().getKeyId(cat.id ?: "") }
    }

    @TypeConverter
    fun fromArrayList(list: List<String?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }


}