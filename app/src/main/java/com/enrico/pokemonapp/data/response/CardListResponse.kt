package com.enrico.pokemonapp.data.response

import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Parcelize
data class CardListResponse(

	@field:SerializedName("data")
	val data: List<DataItem>? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("pageSize")
	val pageSize: Int? = null,

	@field:SerializedName("page")
	val page: Int? = null,

	@field:SerializedName("totalCount")
	val totalCount: Int? = null
) : Parcelable


@Parcelize
data class Images(

	@field:SerializedName("small")
	val small: String? = null,

	@field:SerializedName("large")
	val large: String? = null
) : Parcelable

@Parcelize
data class DataItem(

	@field:SerializedName("supertype")
	val supertype: String? = null,

	@field:SerializedName("types")
	val types: List<String?>? = null,

	@field:SerializedName("images")
	val images: Images? = null,

	@field:SerializedName("artist")
	val artist: String? = null,

	@field:SerializedName("hp")
	val hp: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("level")
	val level: String? = null,
) : Parcelable
