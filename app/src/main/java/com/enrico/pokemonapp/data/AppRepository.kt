package com.enrico.pokemonapp.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.enrico.pokemonapp.api.ApiService
import com.enrico.pokemonapp.data.paging.CardRemoteMediator
import com.enrico.pokemonapp.local.LocalDatabase
import com.enrico.pokemonapp.local.entity.CardEntity
import kotlinx.coroutines.flow.Flow

private const val PAGE_SIZE = 20
class AppRepository(private val localDataBase : LocalDatabase, private val apiService: ApiService) {

    @ExperimentalPagingApi
    fun getListCard(): Flow<PagingData<CardEntity>> {
        return Pager(
            PagingConfig(
                pageSize = PAGE_SIZE,
                initialLoadSize = PAGE_SIZE,
                enablePlaceholders = false,
            ),
            remoteMediator = CardRemoteMediator(
                apiService,
                localDataBase
            ),
            pagingSourceFactory = { localDataBase.cardDao().getCardList() }
        ).flow
    }

    @ExperimentalPagingApi
    fun getListCardSearch(search: String) : Flow<PagingData<CardEntity>> {
        return Pager(
            PagingConfig(
                pageSize = PAGE_SIZE,
                initialLoadSize = PAGE_SIZE,
                enablePlaceholders = false,
            ),
            pagingSourceFactory = { localDataBase.cardDao().getCardSearch("%$search%") }
        ).flow
    }

    suspend fun deleteDummyData() {
        localDataBase.cardDao().deleteAll()
    }
}