package com.enrico.pokemonapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.enrico.pokemonapp.data.AppRepository
import com.enrico.pokemonapp.local.entity.CardEntity
import kotlinx.coroutines.flow.Flow

@ExperimentalPagingApi
class MainViewModel(private val repository: AppRepository) : ViewModel() {

    fun getListCard() : Flow<PagingData<CardEntity>> {
        return repository.getListCard().cachedIn(viewModelScope)
    }

    fun getListSearch(search: String) : Flow<PagingData<CardEntity>> {
        return repository.getListCardSearch(search).cachedIn(viewModelScope)
    }

}