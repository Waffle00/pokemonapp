package com.enrico.pokemonapp.ui

import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.room.TypeConverter
import com.enrico.pokemonapp.databinding.ActivityDetailBinding
import com.enrico.pokemonapp.local.entity.CardEntity
import com.enrico.pokemonapp.utils.loadImage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import xyz.hasnat.sweettoast.SweetToast
import java.io.*
import java.lang.reflect.Type
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.Executors

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        val data = intent.getParcelableExtra<CardEntity>(DATA)
        val color = intent.getStringExtra(COLOR_BACKGROUND)
        binding.apply {
            data?.apply {
                tvNameTitle.text = name
                tvSupertypeTitle.text = superType
                tvNameDetail.text = name
                tvSupertypeDetail.text = superType
                tvTypesDetail.text = types?.replace("[", "")?.replace("]", "")?.replace("\"", "")?.replace(",", ", ")
                tvArtistDetail.text = artist
                tvHpDetail.text = hp
                tvLevelDetail.text = level
                ivCardDetail.loadImage(imageHigh)
                btnBack.setOnClickListener{
                    finish()
                }
                parentImage.setBackgroundColor(Color.parseColor(color))
                val myExecutor = Executors.newSingleThreadExecutor()
                val myHandler = Handler(Looper.getMainLooper())
                btnDownload.setOnClickListener{
                    myExecutor.execute {
                        val mImage = mLoad(imageHigh ?: "")
                        myHandler.post {
                            if(mImage!=null){
                                mSaveMediaToStorage(mImage)
                            }
                        }
                    }
                }
            }

        }
    }

    private fun mLoad(string: String): Bitmap? {
        val url: URL = mStringToURL(string)!!
        val connection: HttpURLConnection?
        try {
            connection = url.openConnection() as HttpURLConnection
            connection.connect()
            val inputStream: InputStream = connection.inputStream
            val bufferedInputStream = BufferedInputStream(inputStream)
            return BitmapFactory.decodeStream(bufferedInputStream)
        } catch (e: IOException) {
            e.printStackTrace()
            SweetToast.error(applicationContext, "Error")
        }
        return null
    }

    private fun mStringToURL(string: String): URL? {
        try {
            return URL(string)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return null
    }

    private fun mSaveMediaToStorage(bitmap: Bitmap?) {
        val filename = "${System.currentTimeMillis()}.jpg"
        var fos: OutputStream? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            this.contentResolver?.also { resolver ->
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }
                val imageUri: Uri? = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            val imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val image = File(imagesDir, filename)
            fos = FileOutputStream(/* file = */ image)
        }
        fos?.use {
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, it)
            SweetToast.success(this, "Saved to Gallery")
        }
    }

    companion object {
        const val DATA = "DetailActivity.data"
        const val COLOR_BACKGROUND = "DetailActivity.color"
    }
}