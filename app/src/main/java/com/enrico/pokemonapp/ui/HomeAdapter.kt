package com.enrico.pokemonapp.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.enrico.pokemonapp.data.response.DataItem
import com.enrico.pokemonapp.databinding.ItemHomeBinding
import com.enrico.pokemonapp.local.entity.CardEntity
import com.enrico.pokemonapp.utils.loadImage

class HomeAdapter: PagingDataAdapter<CardEntity, HomeAdapter.RecyclerViewHolder>(NewsComparator) {
    object NewsComparator : DiffUtil.ItemCallback<CardEntity>() {
        override fun areItemsTheSame(
            oldItem: CardEntity,
            newItem: CardEntity
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: CardEntity,
            newItem: CardEntity
        ): Boolean {
            return oldItem.equals(newItem)
        }

    }

    inner class RecyclerViewHolder(private val binding: ItemHomeBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CardEntity) {
            binding.apply {
                val colorRandom = randomColorGenerator()
                ivCard.loadImage(item.imageSmall)
                tvName.text = item.name
                tvSupertype.text = item.superType
                parentLayout.setBackgroundColor(Color.parseColor(colorRandom))
                root.setOnClickListener{
                    itemView.context.startActivity(Intent(itemView.context, DetailActivity::class.java).putExtra(DetailActivity.DATA, item).putExtra(DetailActivity.COLOR_BACKGROUND, colorRandom))
                }
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val data = getItem(position)!!
        holder.apply {
            bind(data)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val itemBinding = ItemHomeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return RecyclerViewHolder(itemBinding)
    }


    fun randomColorGenerator() : String {
        val list = listOf("#c4e3d4", "#bbd9d7", "#efd3bb", "#e5bbef", "#c2efbb", "#efbbed")
        val randomColor = list.asSequence().shuffled().find { true }
        return randomColor ?: ""
    }

}