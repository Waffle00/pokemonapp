package com.enrico.pokemonapp.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.map
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.TypeConverter
import com.enrico.pokemonapp.data.response.DataItem
import com.enrico.pokemonapp.databinding.ActivityHomeBinding
import com.enrico.pokemonapp.local.entity.CardEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.lang.reflect.Type

@ExperimentalPagingApi
class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding

    private val viewModel: MainViewModel by inject()

    private lateinit var homeAdapter: HomeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        homeAdapter = HomeAdapter()
        binding.apply {
            rvHome.apply {
                layoutManager = GridLayoutManager(this@HomeActivity, 2)
                adapter = homeAdapter
            }
            etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun afterTextChanged(p0: Editable?) {
                    if (p0.isNullOrBlank()) {
                        lifecycleScope.launch {
                            viewModel.getListCard().collectLatest { data ->
                                homeAdapter.submitData(data)
                            }
                        }
                    } else {
                        lifecycleScope.launch {
                            viewModel.getListSearch(etSearch.text.toString()).collectLatest { data ->
                                homeAdapter.submitData(data)
                            }
                        }
                    }
                }

            })
        }
        lifecycleScope.launch {
            viewModel.getListCard().collectLatest { data ->
                homeAdapter.submitData(data)
            }
        }
    }
}