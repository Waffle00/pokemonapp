package com.enrico.pokemonapp.local.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.enrico.pokemonapp.local.entity.CardEntity

@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCardList(list : List<CardEntity>)

    @Query("SELECT * FROM card_db")
    fun getCardList() : PagingSource<Int, CardEntity>

    @Query("SELECT * FROM card_db WHERE name LIKE :search")
    fun getCardSearch(search: String) : PagingSource<Int, CardEntity>

    @Query("DELETE FROM card_db")
    suspend fun deleteAll()
}