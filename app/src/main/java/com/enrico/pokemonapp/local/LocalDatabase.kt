package com.enrico.pokemonapp.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.enrico.pokemonapp.local.dao.CardDao
import com.enrico.pokemonapp.local.dao.KeyDao
import com.enrico.pokemonapp.local.entity.CardEntity
import com.enrico.pokemonapp.local.entity.KeyEntity

@Database(
    entities = [
        CardEntity::class,
        KeyEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class LocalDatabase : RoomDatabase() {
    abstract fun cardDao() : CardDao
    abstract fun keyDao() : KeyDao
}