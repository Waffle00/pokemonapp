package com.enrico.pokemonapp.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "card_db")
class CardEntity (
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name= "id")
    val id: String,
    @ColumnInfo(name = "name")
    val name: String?,
    @ColumnInfo(name = "supertype")
    val superType: String?,
    @ColumnInfo(name = "types")
    val types: String?,
    @ColumnInfo(name = "artist")
    val artist: String?,
    @ColumnInfo(name = "hp")
    val hp: String?,
    @ColumnInfo(name = "level")
    val level: String?,
    @ColumnInfo(name = "image_small")
    val imageSmall: String?,
    @ColumnInfo(name = "image_high")
    val imageHigh: String?
) : Parcelable